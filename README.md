# Welcome to Phortitude!
Phortitude is a PHP class library.

# Release information
Current version: **Phortitude 1.0.0-dev**  
This is the development version of Phortitude 1.0.0.

## Disclaimer
The development on Phortitude has just been started, hence it's in a very early stage and by far not complete. Also, we
can't guarantee that the implemented functions will work bug-free.  
**Don't use Phortitude in production yet!**

This disclaimer will disappear with the first stable release of Phortitude.

# License
Phortitude is licensed under the GNU Lesser General Public License as published by the Free Software Foundation in
version 3. You can read it [online](http://www.gnu.org/licenses/lgpl-3.0-standalone.html) or by having a look in the
[LICENSE.txt](LICENSE.txt) file.

# To be continued ...
More information about Phortitude will be provided in the future. Please stay tuned! :)
